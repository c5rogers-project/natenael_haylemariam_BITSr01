const menuButton = document.querySelector('.menuButton');
const exitButton = document.querySelector('.exitButton');
menuButton.addEventListener("click", () => {
    document.querySelector('.navigation').style.top = "0";
})

exitButton.addEventListener("click", () => {
    document.querySelector('.navigation').style.top = "-20em"
})


getLocation = (number) => {
    switch (number) {
        case 1:
            // document.querySelector('.homeNavigation').style.backgroundColor = "orange";
            // document.querySelector('.homeNavigation').style.color = "aliceblue"
            document.querySelector('.homeNavigation').classList.add('activeLink')
            document.querySelector('.aboutNavigation').classList.remove('activeLink')
            document.querySelector('.testimonialNavigation').classList.remove('activeLink')
            document.querySelector('.contactNavigation').classList.remove('activeLink')
            break;
        case 2:
            // document.querySelector('.aboutNavigation').style.backgroundColor = "orange"
            // document.querySelector('.aboutNavigation').style.color = "aliceblue"
            document.querySelector('.homeNavigation').classList.remove('activeLink')
            document.querySelector('.aboutNavigation').classList.add('activeLink')
            document.querySelector('.testimonialNavigation').classList.remove('activeLink')
            document.querySelector('.contactNavigation').classList.remove('activeLink')
            break;
        case 3:
            // document.querySelector('.testimonialNavigation').style.backgroundColor = "orange"
            // document.querySelector('.testimonialNavigation').style.color = "aliceblue"
            document.querySelector('.homeNavigation').classList.remove('activeLink')
            document.querySelector('.aboutNavigation').classList.remove('activeLink')
            document.querySelector('.testimonialNavigation').classList.add('activeLink')
            document.querySelector('.contactNavigation').classList.remove('activeLink')
            break;
        case 4:
            // document.querySelector('.contactNavigation').style.backgroundColor = "orange"
            // document.querySelector('.contactNavigation').style.color = "aliceblue"
            document.querySelector('.homeNavigation').classList.remove('activeLink')
            document.querySelector('.aboutNavigation').classList.remove('activeLink')
            document.querySelector('.testimonialNavigation').classList.remove('activeLink')
            document.querySelector('.contactNavigation').classList.add('activeLink')
            break;
    }

}
window.onscroll=function (){ScrollAnimation()}

function ScrollAnimation() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  if(scrolled>=20&&scrolled<60){
    getLocation(2)
  }else if(scrolled>=60&& scrolled<80){
    getLocation(3)
  }else if(scrolled>=80){
    getLocation(4)
  }else{
    getLocation(1)
  }
  document.querySelector('.scroll-tracker').style.width=scrolled+'%'
}